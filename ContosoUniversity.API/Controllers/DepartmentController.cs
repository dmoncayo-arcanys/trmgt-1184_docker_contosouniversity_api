﻿using ContosoUniversity.Entities;
using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentService departmentService;

        /// <summary>
        /// Controller.
        /// </summary>
        /// <param name="departmentService"></param>
        public DepartmentController(IDepartmentService departmentService)
        {
            this.departmentService = departmentService;
        }

        /// <summary>
        /// GET: /Departments
        /// </summary>
        /// <returns>IActionResult</returns>
        [HttpGet]
        public IActionResult GetDepartments()
        {
            ApiResultModel results = new();
            results.Response = departmentService.GetAll();
            results.Status = results.Response != null? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// GET: api/Departments/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IActionResult</returns>
        [HttpGet("{id}")]
        public IActionResult GetCourse(int? id)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = departmentService.Get(id);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// POST: api/Departments
        /// </summary>
        /// <param name="department"></param>
        /// <returns>IActionResult</returns>
        [HttpPost]
        public IActionResult PostDepartment(Department department)
        {
            ApiResultModel results = new();
            results.Response = departmentService.Insert(department);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// PUT: api/Departments/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="department"></param>
        /// <returns>IActionResult</returns>
        [HttpPut("{id}")]
        public IActionResult PutCourse(int? id, Department department)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = departmentService.Update(department);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// DELETE: api/Departments/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IActionResult</returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteCourse(int? id)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = departmentService.Delete(id);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }
    }
}
