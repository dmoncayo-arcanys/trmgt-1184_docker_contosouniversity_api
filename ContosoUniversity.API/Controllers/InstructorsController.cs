﻿using ContosoUniversity.Entities;
using ContosoUniversity.Entities.ViewModels;
using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class InstructorsController : ControllerBase
    {
        private readonly IInstructorService instructorService;

        /// <summary>
        /// Controller.
        /// </summary>
        /// <param name="instructorService"></param>
        public InstructorsController(IInstructorService instructorService)
        {
            this.instructorService = instructorService;
        }

        /// <summary>
        /// GET: /Instructors
        /// </summary>
        /// <returns>IActionResult</returns>
        [HttpGet]
        public IActionResult GetInstructors()
        {
            ApiResultModel results = new();
            results.Response = instructorService.GetAll();
            results.Status = results.Response != null? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// GET: api/Instructors/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IActionResult</returns>
        [HttpGet("{id}")]
        public IActionResult GetInstructor(int? id)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = instructorService.Get(id);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// POST: api/Instructors
        /// </summary>
        /// <param name="instructor"></param>
        /// <returns>IActionResult</returns>
        [HttpPost]
        public IActionResult PostInstructor(Instructor instructor)
        {
            ApiResultModel results = new();
            results.Response = instructorService.Insert(instructor);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// PUT: api/Instructors/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="instructor"></param>
        /// <returns>IActionResult</returns>
        [HttpPut("{id}")]
        public IActionResult PutInstructor(int? id, InstructorUpdate instructor)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = instructorService.Update(instructor);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// DELETE: api/Instructors/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IActionResult</returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteInstructor(int? id)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = instructorService.Delete(id);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }
    }
}
