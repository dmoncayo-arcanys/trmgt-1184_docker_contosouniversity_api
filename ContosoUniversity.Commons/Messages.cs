﻿namespace ContosoUniversity.Commons
{
    public class Messages
    {
        public const string ERROR_000 = "No permission to access the data. Please login first.";

        public const string ERROR_001 = "Delete student record failed. Please try again.";
        public const string ERROR_002 = "Delete instructor record failed. Please try again.";
        public const string ERROR_003 = "Delete course record failed. Please try again.";
        public const string ERROR_004 = "Delete department record failed. Please try again.";
        
        public const string ERROR_005 = "Update student record failed. Please try again.";
        public const string ERROR_007 = "Update instructor record failed. Please try again.";
        public const string ERROR_008 = "Update course record failed. Please try again.";
        public const string ERROR_009 = "Update department record failed. Please try again.";

        public const string ERROR_010 = "Creating student record failed. Please try again.";
        public const string ERROR_011 = "Creating instructor record failed. Please try again.";
        public const string ERROR_012 = "Creating course record failed. Please try again.";
        public const string ERROR_013 = "Creating department record failed. Please try again.";
    }
}
