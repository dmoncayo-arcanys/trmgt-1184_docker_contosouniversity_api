﻿namespace ContosoUniversity.Entities.ViewModels
{
    public class InstructorUpdate
    {
        public Instructor Instructors { get; set; }
        public string[] SelectedCourses { get; set; }
    }
}
