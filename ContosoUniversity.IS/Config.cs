﻿using ContosoUniversity.Commons;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace ContosoUniversity.IS
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources() =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
            };

        public static IEnumerable<ApiResource> GetApiResources() =>
            new List<ApiResource>
            {
                new ApiResource(Constants.ASSEMBLY_API)
            };

        public static IEnumerable<ApiScope> GetApiScopes() =>
            new List<ApiScope>
            {
                 new ApiScope(Constants.ASSEMBLY_API)
            };

        public static IEnumerable<Client> GetClients() =>
            new List<Client>
            {
               new Client {
                    ClientId = Constants.CLIENT_REACT,
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,
                    RedirectUris = { Constants.URL_REACT },
                    PostLogoutRedirectUris = { Constants.URL_REACT },
                    AllowedCorsOrigins = { Constants.URL_REACT },
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        Constants.ASSEMBLY_API,
                    },
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,
               },
            };
    }
}
