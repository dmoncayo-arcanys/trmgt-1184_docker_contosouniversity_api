﻿using System.Net;

namespace ContosoUniversity.Models
{
    public class ApiResultModel : BaseApiResultModel
    {
        public string ErrCode { get; set; }

        public string SuccCode { get; set; }

        public HttpStatusCode StatusCode { get; set; }
}
}
