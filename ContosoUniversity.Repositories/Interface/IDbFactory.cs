﻿namespace ContosoUniversity.Repositories.Interface
{
    public interface IDbFactory
    {
        SchoolDbContext GetDbContext { get; }
    }
}
