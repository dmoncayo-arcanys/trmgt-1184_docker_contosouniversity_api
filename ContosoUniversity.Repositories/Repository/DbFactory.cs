﻿using ContosoUniversity.Repositories.Interface;
using System;

namespace ContosoUniversity.Repositories.Repository
{
    public class DbFactory : IDbFactory, IDisposable
    {

        private readonly SchoolDbContext context;

        public DbFactory(SchoolDbContext context)
        {
            this.context = context;
        }

        public SchoolDbContext GetDbContext
        {
            get
            {
                return context;
            }
        }

        private bool isDisposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!isDisposed && disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                }
            }
            isDisposed = true;
        }
    }
}
