﻿using ContosoUniversity.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ContosoUniversity.Repositories.Repository
{
    public class Repository : IRepository
    {
        private readonly SchoolDbContext context;

        public Repository(IDbFactory dbFactory)
        {
            context = dbFactory.GetDbContext;
        }

        public T Single<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return All<T>().FirstOrDefault(expression);
        }

        public IQueryable<T> All<T>() where T : class
        {
            return context.Set<T>().AsQueryable();
        }

        public virtual IEnumerable<T> Filter<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return context.Set<T>().Where<T>(predicate).AsQueryable<T>();
        }

        public virtual IEnumerable<T> Filter<T>(Expression<Func<T, bool>> filter, out int total, int index = 0, int size = 50) where T : class
        {
            int skipCount = index * size;
            var _resetSet = filter != null ? context.Set<T>().Where<T>(filter).AsQueryable() : context.Set<T>().AsQueryable();
            _resetSet = skipCount == 0 ? _resetSet.Take(size) : _resetSet.Skip(skipCount).Take(size);
            total = _resetSet.Count();
            return _resetSet.AsQueryable();
        }

        public virtual void Create<T>(T TObject) where T : class
        {
            context.Set<T>().Add(TObject);
        }

        public virtual void Delete<T>(T TObject) where T : class
        {
            context.Set<T>().Remove(TObject);
        }

        public virtual void Update<T>(T TObject) where T : class
        {
            var entry = context.Entry(TObject);
            context.Set<T>().Attach(TObject);
            entry.State = EntityState.Modified;
        }

        public virtual void Delete<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            var objects = Filter<T>(predicate);
            foreach (var obj in objects)
                context.Set<T>().Remove(obj);
        }

        public bool Contains<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return context.Set<T>().Any(predicate);
        }

        public virtual T Find<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return context.Set<T>().FirstOrDefault<T>(predicate);
        }

        public virtual void ExecuteProcedure(String procedureCommand, params object[] sqlParams)
        {
            context.Database.ExecuteSqlRaw(procedureCommand, sqlParams);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
