﻿using ContosoUniversity.Repositories.Interface;

namespace ContosoUniversity.Repositories.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory dbFactory;

        public UnitOfWork(IDbFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }

        public void BeginTransaction()
        {
            dbFactory.GetDbContext.Database.BeginTransaction();
        }

        public void RollbackTransaction()
        {
            dbFactory.GetDbContext.Database.RollbackTransaction();
        }

        public void CommitTransaction()
        {
            dbFactory.GetDbContext.Database.CommitTransaction();
        }

        public void SaveChanges()
        {
            dbFactory.GetDbContext.Save();
        }
    }
}
