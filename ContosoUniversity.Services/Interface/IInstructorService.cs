﻿using ContosoUniversity.Entities;
using ContosoUniversity.Entities.ViewModels;
using ContosoUniversity.Services.Core;

namespace ContosoUniversity.Services.Interface
{
    public interface IInstructorService : IActionManager<Instructor>
    {
        bool Update(InstructorUpdate instructor);
    }
}
