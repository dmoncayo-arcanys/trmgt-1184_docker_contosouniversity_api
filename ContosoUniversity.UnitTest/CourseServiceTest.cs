using ContosoUniversity.Entities;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Service;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace ContosoUniversity.UnitTest
{
    public class CourseServiceTest
    {
        private readonly CourseService courseService;
        private readonly Mock<IUnitOfWork> unitOfWork = new();
        private readonly Mock<IRepository> repository = new();
        private readonly Mock<ILogger<CourseService>> logger = new();

        public CourseServiceTest()
        {
            courseService = new CourseService(unitOfWork.Object, repository.Object, logger.Object);
        }

        [Fact]
        public void Insert_ShouldReturnTrue_WhenDataIsValid()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            // Act
            var results = courseService.Insert(course);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Insert_ShouldReturnFalse_WhenDataIsNotValid()
        {
            // Act
            var results = courseService.Insert(null);

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void GetAll_ShouldReturnValue_WhenDataExist()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            IQueryable<Course> query = Enumerable.Empty<Course>().AsQueryable();
            query = query.Concat(new Course[] { course });

            repository.Setup(x => x.All<Course>()).Returns(query);

            // Act
            var results = courseService.GetAll();

            // Assert
            Assert.NotNull(results);
        }

        [Fact]
        public void GetAll_ShouldReturnValue_WhenDataNotExist()
        {
            // Arrange
            IQueryable<Course> query = null;

            repository.Setup(x => x.All<Course>()).Returns(query);

            // Act
            var results = courseService.GetAll();

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Get_ShouldReturnValue_WhenDataIsFound()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            IQueryable<Course> query = Enumerable.Empty<Course>().AsQueryable();
            query = query.Concat(new Course[] { course });

            repository.Setup(x => x.All<Course>()).Returns(query);

            // Act
            var results = courseService.Get(courseId);

            // Assert
            Assert.Equal(courseId, results.CourseID);
        }

        [Fact]
        public void Get_ShouldReturnNull_WhenDataNotFound()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = 2,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            IQueryable<Course> query = Enumerable.Empty<Course>().AsQueryable();
            query = query.Concat(new Course[] { course });

            repository.Setup(x => x.All<Course>()).Returns(query);

            // Act
            var results = courseService.Get(courseId);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Get_ShouldReturnNull_WhenParameterIsInvalid()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            IQueryable<Course> query = Enumerable.Empty<Course>().AsQueryable();
            query = query.Concat(new Course[] { course });

            repository.Setup(x => x.All<Course>()).Returns(query);

            // Act
            var results = courseService.Get(null); // Parameter is null.

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Delete_ShouldReturnTrue_WhenItemExist()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            IQueryable<Course> query = Enumerable.Empty<Course>().AsQueryable();
            query = query.Concat(new Course[] { course });

            repository.Setup(x => x.All<Course>()).Returns(query);
            repository.Setup(x => x.Delete(course));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = courseService.Delete(courseId);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Delete_ShouldReturnFalse_WhenItemNotExist()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            IQueryable<Course> query = Enumerable.Empty<Course>().AsQueryable();
            query = query.Concat(new Course[] { course });

            repository.Setup(x => x.All<Course>()).Returns(query);
            repository.Setup(x => x.Delete(course));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = courseService.Delete(2); // Different from 'courseId'

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Delete_ShouldReturnFalse_WhenParameterIsInvalid()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            IQueryable<Course> query = Enumerable.Empty<Course>().AsQueryable();
            query = query.Concat(new Course[] { course });

            repository.Setup(x => x.All<Course>()).Returns(query);
            repository.Setup(x => x.Delete(course));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = courseService.Delete(null); // Parameter is null.

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Update_ShouldReturnTrue_WhenItemExist()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            IQueryable<Course> query = Enumerable.Empty<Course>().AsQueryable();
            query = query.Concat(new Course[] { course });

            repository.Setup(x => x.All<Course>()).Returns(query);
            repository.Setup(x => x.Update(course));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = courseService.Update(course);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Update_ShouldReturnFalse_WhenItemNotExist()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            var courseUpdate = new Course
            {
                CourseID = 2,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            IQueryable<Course> query = Enumerable.Empty<Course>().AsQueryable();
            query = query.Concat(new Course[] { course });

            repository.Setup(x => x.All<Course>()).Returns(query);
            repository.Setup(x => x.Update(course));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = courseService.Update(courseUpdate);

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Update_ShouldReturnFalse_WhenParameterIsInvalid()
        {
            // Arrange
            var courseId = 1;

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            IQueryable<Course> query = Enumerable.Empty<Course>().AsQueryable();
            query = query.Concat(new Course[] { course });

            repository.Setup(x => x.All<Course>()).Returns(query);
            repository.Setup(x => x.Update(course));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = courseService.Update(null);

            // Assert
            Assert.False(results);
        }
    }
}
