using ContosoUniversity.Entities;
using ContosoUniversity.Entities.ViewModels;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Service;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace ContosoUniversity.UnitTest
{
    public class InstructorServiceTest
    {
        private readonly InstructorService instructorService;
        private readonly Mock<IUnitOfWork> unitOfWork = new();
        private readonly Mock<IRepository> repository = new();
        private readonly Mock<ILogger<InstructorService>> logger = new();

        public InstructorServiceTest()
        {
            instructorService = new InstructorService(unitOfWork.Object, repository.Object, logger.Object);
        }

        [Fact]
        public void Insert_ShouldReturnTrue_WhenDataIsValid()
        {
            // Arrange
            var instructorId = 1;

            var instructor = new Instructor
            {
                ID = instructorId,
                FirstMidName = "",
                LastName = "",
                HireDate = DateTime.Now,
            };

            // Act
            var results = instructorService.Insert(instructor);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Insert_ShouldReturnFalse_WhenDataIsNotValid()
        {
            // Act
            var results = instructorService.Insert(null);

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void GetAll_ShouldReturnValue_WhenDataExist()
        {
            // Arrange
            var instructorId = 1;

            var instructor = new Instructor
            {
                ID = instructorId,
                FirstMidName = "",
                LastName = "",
                HireDate = DateTime.Now,
            };

            IQueryable<Instructor> query = Enumerable.Empty<Instructor>().AsQueryable();
            query = query.Concat(new Instructor[] { instructor });

            repository.Setup(x => x.All<Instructor>()).Returns(query);

            // Act
            var results = instructorService.GetAll();

            // Assert
            Assert.NotNull(results);
        }

        [Fact]
        public void GetAll_ShouldReturnValue_WhenDataNotExist()
        {
            // Arrange
            IQueryable<Instructor> query = null;

            repository.Setup(x => x.All<Instructor>()).Returns(query);

            // Act
            var results = instructorService.GetAll();

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Get_ShouldReturnValue_WhenDataIsFound()
        {
            // Arrange
            var instructorId = 1;

            var instructor = new Instructor
            {
                ID = instructorId,
                FirstMidName = "",
                LastName = "",
                HireDate = DateTime.Now,
            };

            IQueryable<Instructor> query = Enumerable.Empty<Instructor>().AsQueryable();
            query = query.Concat(new Instructor[] { instructor });

            repository.Setup(x => x.All<Instructor>()).Returns(query);

            // Act
            var results = instructorService.Get(instructorId);

            // Assert
            Assert.Equal(instructorId, results.ID);
        }

        [Fact]
        public void Get_ShouldReturnNull_WhenDataNotFound()
        {
            // Arrange
            var instructorId = 1;

            var instructor = new Instructor
            {
                ID = 2,
                FirstMidName = "",
                LastName = "",
                HireDate = DateTime.Now,
            };

            IQueryable<Instructor> query = Enumerable.Empty<Instructor>().AsQueryable();
            query = query.Concat(new Instructor[] { instructor });

            repository.Setup(x => x.All<Instructor>()).Returns(query);

            // Act
            var results = instructorService.Get(instructorId);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Get_ShouldReturnNull_WhenParameterIsInvalid()
        {
            // Arrange
            var instructorId = 1;

            var instructor = new Instructor
            {
                ID = instructorId,
                FirstMidName = "",
                LastName = "",
                HireDate = DateTime.Now,
            };

            IQueryable<Instructor> query = Enumerable.Empty<Instructor>().AsQueryable();
            query = query.Concat(new Instructor[] { instructor });

            repository.Setup(x => x.All<Instructor>()).Returns(query);

            // Act
            var results = instructorService.Get(null); // Parameter is null.

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Delete_ShouldReturnTrue_WhenItemExist()
        {
            // Arrange
            var instructorId = 1;

            var instructor = new Instructor
            {
                ID = instructorId,
                FirstMidName = "",
                LastName = "",
                HireDate = DateTime.Now,
            };

            IQueryable<Instructor> query = Enumerable.Empty<Instructor>().AsQueryable();
            query = query.Concat(new Instructor[] { instructor });

            repository.Setup(x => x.All<Instructor>()).Returns(query);
            repository.Setup(x => x.Delete(instructor));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = instructorService.Delete(instructorId);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Delete_ShouldReturnFalse_WhenItemNotExist()
        {
            // Arrange
            var instructorId = 1;

            var instructor = new Instructor
            {
                ID = instructorId,
                FirstMidName = "",
                LastName = "",
                HireDate = DateTime.Now,
            };

            IQueryable<Instructor> query = Enumerable.Empty<Instructor>().AsQueryable();
            query = query.Concat(new Instructor[] { instructor });

            repository.Setup(x => x.All<Instructor>()).Returns(query);
            repository.Setup(x => x.Delete(instructor));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = instructorService.Delete(2); // Different from 'instructorId'

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Delete_ShouldReturnFalse_WhenParameterIsInvalid()
        {
            // Arrange
            var instructorId = 1;

            var instructor = new Instructor
            {
                ID = instructorId,
                FirstMidName = "",
                LastName = "",
                HireDate = DateTime.Now,
            };

            IQueryable<Instructor> query = Enumerable.Empty<Instructor>().AsQueryable();
            query = query.Concat(new Instructor[] { instructor });

            repository.Setup(x => x.All<Instructor>()).Returns(query);
            repository.Setup(x => x.Delete(instructor));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = instructorService.Delete(null); // Parameter is null.

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Update_ShouldReturnTrue_WhenItemExist()
        {
            // Arrange
            var instructorId = 1;
            var courseId = 1;

            var instructor = new Instructor
            {
                ID = instructorId,
                FirstMidName = "",
                LastName = "",
                HireDate = DateTime.Now,
            };

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            var holder = new InstructorUpdate
            {
                Instructors = instructor,
            };

            IQueryable<Instructor> query = Enumerable.Empty<Instructor>().AsQueryable();
            query = query.Concat(new Instructor[] { instructor });

            IQueryable<Course> queryCourse = Enumerable.Empty<Course>().AsQueryable();
            queryCourse = queryCourse.Concat(new Course[] { course });

            repository.Setup(x => x.All<Instructor>()).Returns(query);
            repository.Setup(x => x.All<Course>()).Returns(queryCourse);

            repository.Setup(x => x.Update(instructor));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = instructorService.Update(holder);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Update_ShouldReturnFalse_WhenItemNotExist()
        {
            // Arrange
            var instructorId = 1;
            var courseId = 1;

            var instructor = new Instructor
            {
                ID = instructorId,
                FirstMidName = "",
                LastName = "",
                HireDate = DateTime.Now,
            };

            var course = new Course
            {
                CourseID = courseId,
                Title = "",
                Credits = 3,
                DepartmentID = 1,
            };

            var holder = new InstructorUpdate
            {
                Instructors = null,
            };

            IQueryable<Instructor> query = Enumerable.Empty<Instructor>().AsQueryable();
            query = query.Concat(new Instructor[] { instructor });

            IQueryable<Course> queryCourse = Enumerable.Empty<Course>().AsQueryable();
            queryCourse = queryCourse.Concat(new Course[] { course });

            repository.Setup(x => x.All<Instructor>()).Returns(query);
            repository.Setup(x => x.All<Course>()).Returns(queryCourse);

            repository.Setup(x => x.Update(instructor));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = instructorService.Update(holder);

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Update_ShouldReturnFalse_WhenParameterIsInvalid()
        {
            //// Arrange
            //var instructorId = 1;

            //var instructor = new Instructor
            //{
            //    ID = instructorId,
            //    FirstMidName = "",
            //    LastName = "",
            //    HireDate = DateTime.Now,
            //};

            //IQueryable<Instructor> query = Enumerable.Empty<Instructor>().AsQueryable();
            //query = query.Concat(new Instructor[] { instructor });

            //repository.Setup(x => x.All<Instructor>()).Returns(query);
            //repository.Setup(x => x.Update(instructor));
            //unitOfWork.Setup(x => x.SaveChanges());

            //// Act
            //var results = instructorService.Update(null);

            //// Assert
            //Assert.False(results);
        }
    }
}
