using ContosoUniversity.Entities;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Service;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace ContosoUniversity.UnitTest
{
    public class StudentServiceTest
    {
        private readonly StudentService studentService;
        private readonly Mock<IUnitOfWork> unitOfWork = new();
        private readonly Mock<IRepository> repository = new();
        private readonly Mock<ILogger<StudentService>> logger = new();

        public StudentServiceTest()
        {
            studentService = new StudentService(unitOfWork.Object, repository.Object, logger.Object);
        }

        [Fact]
        public void Insert_ShouldReturnTrue_WhenDataIsValid()
        {
            // Arrange
            var studentId = 1;
            var studentFirstName = "";
            var studentLastName = "";
            var studentEnrollmentDate = DateTime.Now;

            var student = new Student
            {
                ID = studentId,
                FirstMidName = studentFirstName,
                LastName = studentLastName,
                EnrollmentDate = studentEnrollmentDate,
            };

            // Act
            var results = studentService.Insert(student);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Insert_ShouldReturnFalse_WhenDataIsNotValid()
        {
            // Act
            var results = studentService.Insert(null);

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void GetAll_ShouldReturnValue_WhenDataExist()
        {
            // Arrange
            var studentId = 1;
            var studentFirstName = "";
            var studentLastName = "";
            var studentEnrollmentDate = DateTime.Now;

            var student = new Student
            {
                ID = studentId,
                FirstMidName = studentFirstName,
                LastName = studentLastName,
                EnrollmentDate = studentEnrollmentDate,
            };

            IQueryable<Student> query = Enumerable.Empty<Student>().AsQueryable();
            query = query.Concat(new Student[] { student });

            repository.Setup(x => x.All<Student>()).Returns(query);

            // Act
            var results = studentService.GetAll();

            // Assert
            Assert.NotNull(results);
        }

        [Fact]
        public void GetAll_ShouldReturnValue_WhenDataNotExist()
        {
            // Arrange
            IQueryable<Student> query = null;

            repository.Setup(x => x.All<Student>()).Returns(query);

            // Act
            var results = studentService.GetAll();

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Get_ShouldReturnValue_WhenDataIsFound()
        {
            // Arrange
            var studentId = 1;

            var student = new Student
            {
                ID = studentId,
            };

            IQueryable<Student> query = Enumerable.Empty<Student>().AsQueryable();
            query = query.Concat(new Student[] { student });

            repository.Setup(x => x.All<Student>()).Returns(query);

            // Act
            var results = studentService.Get(studentId);

            // Assert
            Assert.Equal(studentId, results.ID);
        }

        [Fact]
        public void Get_ShouldReturnNull_WhenDataNotFound()
        {
            // Arrange
            var studentId = 1;

            var student = new Student
            {
                ID = 2, // ID is different from 'studentId'
            };

            IQueryable<Student> query = Enumerable.Empty<Student>().AsQueryable();
            query = query.Concat(new Student[] { student });

            repository.Setup(x => x.All<Student>()).Returns(query);

            // Act
            var results = studentService.Get(studentId);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Get_ShouldReturnNull_WhenParameterIsInvalid()
        {
            // Arrange
            var studentId = 1;

            var student = new Student
            {
                ID = studentId,
            };

            IQueryable<Student> query = Enumerable.Empty<Student>().AsQueryable();
            query = query.Concat(new Student[] { student });

            repository.Setup(x => x.All<Student>()).Returns(query);

            // Act
            var results = studentService.Get(null); // Parameter is null.

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Delete_ShouldReturnTrue_WhenItemExist()
        {
            // Arrange
            var studentId = 1;
            var studentFirstName = "";
            var studentLastName = "";
            var studentEnrollmentDate = DateTime.Now;

            var student = new Student
            {
                ID = studentId,
                FirstMidName = studentFirstName,
                LastName = studentLastName,
                EnrollmentDate = studentEnrollmentDate,
            };

            IQueryable<Student> query = Enumerable.Empty<Student>().AsQueryable();
            query = query.Concat(new Student[] { student });

            repository.Setup(x => x.All<Student>()).Returns(query);
            repository.Setup(x => x.Delete(student));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = studentService.Delete(studentId);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Delete_ShouldReturnFalse_WhenItemNotExist()
        {
            // Arrange
            var studentId = 1;
            var studentFirstName = "";
            var studentLastName = "";
            var studentEnrollmentDate = DateTime.Now;

            var student = new Student
            {
                ID = studentId,
                FirstMidName = studentFirstName,
                LastName = studentLastName,
                EnrollmentDate = studentEnrollmentDate,
            };

            IQueryable<Student> query = Enumerable.Empty<Student>().AsQueryable();
            query = query.Concat(new Student[] { student });

            repository.Setup(x => x.All<Student>()).Returns(query);
            repository.Setup(x => x.Delete(student));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = studentService.Delete(2); // Different from 'studentId'

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Delete_ShouldReturnFalse_WhenParameterIsInvalid()
        {
            // Arrange
            var studentId = 1;
            var studentFirstName = "";
            var studentLastName = "";
            var studentEnrollmentDate = DateTime.Now;

            var student = new Student
            {
                ID = studentId,
                FirstMidName = studentFirstName,
                LastName = studentLastName,
                EnrollmentDate = studentEnrollmentDate,
            };

            IQueryable<Student> query = Enumerable.Empty<Student>().AsQueryable();
            query = query.Concat(new Student[] { student });

            repository.Setup(x => x.All<Student>()).Returns(query);
            repository.Setup(x => x.Delete(student));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = studentService.Delete(null); // Parameter is null.

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Update_ShouldReturnTrue_WhenItemExist()
        {
            // Arrange
            var studentId = 1;
            var studentFirstName = "";
            var studentLastName = "";
            var studentEnrollmentDate = DateTime.Now;

            var student = new Student
            {
                ID = studentId,
                FirstMidName = studentFirstName,
                LastName = studentLastName,
                EnrollmentDate = studentEnrollmentDate,
            };

            IQueryable<Student> query = Enumerable.Empty<Student>().AsQueryable();
            query = query.Concat(new Student[] { student });

            repository.Setup(x => x.All<Student>()).Returns(query);
            repository.Setup(x => x.Update(student));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = studentService.Update(student);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Update_ShouldReturnFalse_WhenItemNotExist()
        {
            // Arrange
            var studentId = 1;
            var studentFirstName = "";
            var studentLastName = "";
            var studentEnrollmentDate = DateTime.Now;

            var student = new Student
            {
                ID = studentId,
                FirstMidName = studentFirstName,
                LastName = studentLastName,
                EnrollmentDate = studentEnrollmentDate,
            };

            var studentUpdate = new Student
            {
                ID = 2,
                FirstMidName = studentFirstName,
                LastName = studentLastName,
                EnrollmentDate = studentEnrollmentDate,
            };

            IQueryable<Student> query = Enumerable.Empty<Student>().AsQueryable();
            query = query.Concat(new Student[] { student });

            repository.Setup(x => x.All<Student>()).Returns(query);
            repository.Setup(x => x.Update(student));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = studentService.Update(studentUpdate);

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Update_ShouldReturnFalse_WhenParameterIsInvalid()
        {
            // Arrange
            var studentId = 1;
            var studentFirstName = "";
            var studentLastName = "";
            var studentEnrollmentDate = DateTime.Now;

            var student = new Student
            {
                ID = studentId,
                FirstMidName = studentFirstName,
                LastName = studentLastName,
                EnrollmentDate = studentEnrollmentDate,
            };

            IQueryable<Student> query = Enumerable.Empty<Student>().AsQueryable();
            query = query.Concat(new Student[] { student });

            repository.Setup(x => x.All<Student>()).Returns(query);
            repository.Setup(x => x.Update(student));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = studentService.Update(null);

            // Assert
            Assert.False(results);
        }
    }
}
