# Contoso University: API and IdentityServer4 (Training)

### Requirements ###
* [Download Docker](https://www.docker.com/products/docker-desktop).

### How do I get set up? ###

* Build and execute migration (See Migration for ContosoUniversity.API and ContosoUniversity.IS).
* Right click Project Solution and go to Properties.
* Under Common Properties > Startup Project, select Multiple startup project and set Start to ContosoUniversity.API and ContosoUniversity.IS.
* For ContosoUniversity.API and ContosoUniversity.IS, set Debug to Docker profile.
* Run Project in Visual Studio.
* Docker Images and Containers will be created.

#### Migration for ContosoUniversity.API ####

* In CMD, go to `cd ContosoUniversity.API\ContosoUniversity.API`
* Run in CMD `dotnet ef migrations add InitialCreate`.
* Run in CMD `dotnet ef database update`

#### Migration for ContosoUniversity.IS ####

* In CMD, go to `cd ContosoUniversity.API\ContosoUniversity.IS`
* Run in CMD `dotnet ef migrations add InitialMigration -c AuthDbContext`.
* Run in CMD `dotnet ef migrations add InitialIdentityServerPersistedGrantDbMigration -c AuthPersistedGrantDbContext -o Migrations/IdentityServer/PersistedGrantDb`
* Run in CMD `dotnet ef migrations add InitialIdentityServerConfigurationDbMigration -c AuthConfigurationDbContext -o Migrations/IdentityServer/ConfigurationDb`

### What's next? ###
* Setup [Contoso University React Client](https://bitbucket.org/dmoncayo-arcanys/trmgt-1184_docker_contosouniversity_react/src/master/).

